; M-x eval-buffer  OR  C-e C-x C-e on individual lines

;; Packages
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
;(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))
;(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(package-initialize)

;; Local lisp code
(add-to-list 'load-path "~/.elisp/")

;; Save and restore Emacs's state
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Saving-Emacs-Sessions.html#Saving-Emacs-Sessions
(desktop-save-mode 1)

;; Org-based configuration file
;; https://github.com/daedreth/UncleDavesEmacs#reloading-the-configuration
(defun my-config-reload ()
  "Reloads ~/.emacs.d/config.org at runtime"
  (interactive)
  (org-babel-load-file (expand-file-name "~/.emacs.d/config.org")))
;(global-set-key (kbd "C-c r") 'my-config-reload)
(my-config-reload) ;; do it
